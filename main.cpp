#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

template <typename T>
struct Array2D {

    T* array;
    unsigned int width;
    unsigned int height;

    Array2D(unsigned int w, unsigned int h) {
        array = new T[w*h];
    }
    Array2D(const Array2D& other) {
        array = new T[other.width*other.height];

        for (int i = 0; i < width*height; i++)
            array[i] = other.array[i];
    }
    ~Array2D() {
        delete[] array;
    }

    T& operator()(int x, int y) {
        return array[width*y + x];
    }
    Array2D& operator=(const Array2D& other) {
        delete[] array;

        width = other.width;
        height = other.height;
        array = new T[other.width*other.height];

        for (int i = 0; i < width*height; i++)
            array[i] = other.array[i];

        return *this;
    }
};

#define ANSI_RED     "\x1b[31m"
#define ANSI_GREEN   "\x1b[32m"
#define ANSI_YELLOW  "\x1b[33m"
#define ANSI_BLUE    "\x1b[34m"
#define ANSI_MAGENTA "\x1b[35m"
#define ANSI_CYAN    "\x1b[36m"
#define ANSI_RESET   "\x1b[0m"

#define   ROCK    'r'
#define P_ROCK     0.1*RAND_MAX

#define   ROOT    '.'
#define   SAPLING 't'
#define   TREE    'T'

void draw_trees(Array2D<char> trees) {
    char* print_buffer = new char[trees.width*(2+10)*trees.height*(2+10)+1];

    for (int x = 0; x < trees.width; x++) {
        for (int y = 0; y < trees.height; y++) {
            char tile = trees(x,y);
            switch (tile) {
                case ROCK:
                    //print_buffer += (ROCK + ' ' + ANSI_MAGENTA);
                    break;
                case ROOT:
                    //print_buffer += (ROOT + ' ' + ANSI_GREEN);
                    break;
            }
            //print_buffer += (ANSI_RESET + '\n');
        }
    }

    delete[] print_buffer;
}

int main(int argc, char* argv[]) {
    unsigned int width,
                 height;

    if (argc == 0+1) {
        width = 40;
        height = 40;
    } else if (argc == 2+1) {
        sscanf(argv[1], "%u", &width);
        sscanf(argv[2], "%u", &height);
    } else {
        printf("Requires 2 arguments.\n");
        exit(0);
    }

    // Init trees.
    Array2D<char> trees(width, height);
    Array2D<char> copy_buffer = trees;
    for (unsigned int x = 0; x < width; x++) {
        for (unsigned int y = 0; y < height; y++) {
            if (rand() < P_ROCK)
                trees(x,y) = ROCK;
            else
                trees(x,y) = ROOT;
        }
    }

    // Loop.
    while (true) {

        // Update.
        for (unsigned int x = 0; x < width; x++) {
            for (unsigned int y = 0; y < height; y++) {

            }
        }

        // Swap buffers.
        // TODO with some pointer shit, we wouldn't have to copy the buffers every frame.
        trees = copy_buffer;

        // Clear.

        // Draw.
        draw_trees(trees);

        // Sleep.
        struct timespec ts;
        ts.tv_sec = 0;
        ts.tv_nsec = 100e6;
        nanosleep(&ts,NULL);

    }
}
