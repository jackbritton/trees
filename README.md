Very simple program I wrote a while ago in C. It simulates a forest fire: trees pop up, burn down, and fires spread in these big waves.

Should compile on anything. There is color with ANSI color codes.. if you're on Windows, you may want to disable those. I haven't tested to see whether it's a problem or not.
