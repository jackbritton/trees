CC=g++
TARGET=trees

.PHONY: trees
trees :
	$(CC) *.cpp -o $(TARGET)
