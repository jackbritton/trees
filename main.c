#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>

//colors
#define ANSI_RED     "\x1b[31m"
#define ANSI_GREEN   "\x1b[32m"
#define ANSI_YELLOW  "\x1b[33m"
#define ANSI_BLUE    "\x1b[34m"
#define ANSI_MAGENTA "\x1b[35m"
#define ANSI_CYAN    "\x1b[36m"
#define ANSI_RESET   "\x1b[0m"

//array
#define WIDTH 39
#define HEIGHT 24
char trees[WIDTH][HEIGHT] = {0};
char buffer[WIDTH][HEIGHT] = {0};
#define PRINT_BUFFER_LENGTH WIDTH*(2+10)*HEIGHT*(2+10)+1
char print_buffer[PRINT_BUFFER_LENGTH];

//new lines required to clear
#define LINES_TO_CLEAR 0

//char types
#define ROCK 'r'
#define FIRE 'B'
#define EMBER 'b'
#define ROOT '.'
#define SAPLING 't'
#define TREE 'T'

//probabilities
//these are multiplied by RAND_MAX so they can be directly compared with rand() without having to divide by RAND_MAX.. big performance boost!
#define P_ROCK 0.1*RAND_MAX
#define P_GROW 0.005*RAND_MAX
#define P_BURN 0.0001*RAND_MAX
#define P_EXTINGUISH 0.2*RAND_MAX

char can_burn(char n) {
    return (n == TREE || n == SAPLING);
}

//init array
void init(void) {
    srand(time(0));
    for (char x = 0; x < WIDTH; x++) {
        for (char y = 0; y < HEIGHT; y++) {
            unsigned int p = rand();
            if (p < P_ROCK) {
                trees[x][y] = ROCK;
            } else {
                trees[x][y] = ROOT;
            }
            buffer[x][y] = trees[x][y];
        }
    }
}

//drawing
//first, fill the print buffer
void fill_print_buffer(void) {
    print_buffer[0] = '\0';
    char icon[3];
    icon[1] = ' ';
    icon[2] = '\0';
    char color[6];
    for (char y = 0; y < HEIGHT; y++) {
        for (char x = 0; x < WIDTH; x++) {
            icon[0] = trees[x][y];
            switch (trees[x][y]) {
                case ROCK:
                    strcpy(color, ANSI_MAGENTA);
                    break;
                case ROOT:
                    strcpy(color, ANSI_GREEN);
                    break;
                case SAPLING:
                    strcpy(color, ANSI_GREEN);
                    break;
                case TREE:
                    strcpy(color, ANSI_GREEN);
                    break;
                case FIRE:
                    strcpy(color, ANSI_RED);
                    break;
                case EMBER:
                    strcpy(color, ANSI_RED);
                    break;
            }
            strcat(print_buffer, color);
            strcat(print_buffer, icon);
            strcat(print_buffer, ANSI_RESET);
        }
        strcat(print_buffer, "\n");
    }
}
//draw it
void draw(void) {
    printf("%s", print_buffer);
}

void clear(void) {
    for (unsigned int i = 0; i < LINES_TO_CLEAR; i++) {
        printf("\n");
    }
}

void swap_buffers(void) {
    for (char x = 0; x < WIDTH; x++) {
        for (char y = 0; y < HEIGHT; y++) {
            trees[x][y] = buffer[x][y];
        }
    }
}

//update a partition of the array (through the buffer, that is!)
//partitions lend themselves (hopefully) to multithreading!
void update_partition(char x, char y, char w, char h) {
    //check each fire tile for trees in its cardinal directions
    char keep_going;
    for (char xn = x; xn - x < w; xn++) {
        for (char yn = y; yn - y < h; yn++) {
            if (trees[xn][yn] == FIRE) {
                //check a) if the given adjacent tile is on the board, b) if that tile is on fire
                keep_going = 1;
                if (xn - 1 >= 0) {
                    if (can_burn(trees[xn-1][yn])) {
                        buffer[xn-1][yn] = FIRE;
                        keep_going = 0;
                    }
                }
                if (keep_going && xn + 1 < WIDTH) {
                    if (can_burn(trees[xn+1][yn])) {
                        buffer[xn+1][yn] = FIRE;
                        keep_going = 0;
                    }
                }
                if (keep_going && yn - 1 >= 0) {
                    if (can_burn(trees[xn][yn-1])) {
                        buffer[xn][yn-1] = FIRE;
                        keep_going = 0;
                    }
                }
                if (keep_going && yn + 1 < HEIGHT) {
                    if (can_burn(trees[xn][yn+1])) {
                        buffer[xn][yn+1] = FIRE;
                        keep_going = 0;
                    }
                }
            }
        }
    }

    //chance to grow, burn, etc.
    for (char xn = x; xn - x < w; xn++) {
        for (char yn = y; yn - y < h; yn++) {
            unsigned int p = rand();
            switch (trees[xn][yn]) {
                case EMBER:
                    if (p < P_EXTINGUISH) {
                        buffer[xn][yn] = ROOT;
                    }
                    break;
                case FIRE:
                    if (p < P_EXTINGUISH) {
                        buffer[xn][yn] = EMBER;
                    }
                    break;
                case TREE:
                    if (p < P_BURN) {
                        buffer[xn][yn] = FIRE;
                    }
                    break;
                case SAPLING:
                    if (p < P_GROW) {
                        buffer[xn][yn] = TREE;
                    }
                    break;
                case ROOT:
                    if (p < P_GROW) {
                        buffer[xn][yn] = SAPLING;
                    }
                    break;
            }
        }
    }
}

int main(void) {
    init();
    while (1) {
        update_partition(0,0,WIDTH,HEIGHT);

        swap_buffers();
        fill_print_buffer();
        clear();
        draw();

        //sleep
        struct timespec ts;
        ts.tv_sec = 0;
        ts.tv_nsec = 100e6;
        nanosleep(&ts,NULL);
    }
}
